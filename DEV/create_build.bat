@Echo off
@echo Starting build
@echo Compressing files to archive
REM Backup the files using the variable to insert date in name of backup archive.
"C:\Program Files\7-Zip\7z.exe" a "%~dp0\builds\BMod_%time:~0,2%%time:~3,2%%time:~6,2%_%date:~-10,2%%date:~-7,2%%date:~-4,4%.zip" "%~dp0\..\DATA\" "%~dp0\..\DLLS\" "%~dp0\..\EXE\" "%~dp0\..\script.xml"

@Echo Build finished