# Getting Started 

BeagleMod is a multiplayer overhaul for Freelancer. This mod is built on top of Arithmos, a single-player focused difficulty overhaul by IrateRedKite, which you should check out [here.](https://gitlab.com/IrateRedKite/ene-freelancer/-/tree/0.7-vanilla-plus-sp)

The mod makes significant changes to the flight model, economy, open-space areas of the game and the UI. A comprehensive technical changelog can be found [here.](https://gitlab.com/IrateRedKite/ene-freelancer/-/tree/0.7-vanilla-plus-sp) A workıng document with weapon, shıp and equipment stats can be found [here.](https://docs.google.com/spreadsheets/d/1hWt1DwkQ8G39P5a8vcUjhf--SkmsRTdbrvjN6dDJL44/edit#gid=2061312478)

There is also a modified version of the [Freelancer Swag Pack](https://discoverygc.com/forums/showthread.php?tid=98156) that has been optimised to run with BeagleMod. It can be found [here.](https://drive.google.com/open?id=1s-KW4o953n8ls5AyAaOlwXzaERH4Ahz5) Beagle's Discord Server can be found [here.](https://discord.gg/qMHm9gb)

## INSTALLATION

1. [Download and extract Freelancer.zip](https://drive.google.com/open?id=1OLK41ObRxmhFOBl1splcxftpqrJoIcky) to the default location.
2. Download and run [flpatch.exe](https://gitlab.com/IrateRedKite/ene-freelancer/-/raw/0.92-second-circle/DEV/flpatch.exe)
3. Download and run [jflp.exe](https://gitlab.com/IrateRedKite/ene-freelancer/-/raw/0.92-second-circle/DEV/jflp.exe)
4. Download and install Freelancer Mod Manager 2.0 as administrator from [here](https://the-starport.net/freelancer/download/visit.php?cid=1&lid=2706)
5. Download and run the latest (0.91c) [.flmod file for the mod](https://gitlab.com/IrateRedKite/ene-freelancer/-/raw/0.92-second-circle/DEV/BeagleMod%200.91c.flmod?inline=false), and then enable it in FLMM. (You may need to run Freelancer.exe and FLMM as admin to avoid issues)
6. You should now be able to connect to the MJB 24/7 Server to play online.

## MULTIPLAYER

On many modern systems, Freelancer will not generate a multiplayer ID correctly. In order to fix this you will need to do the following.

1. Close Freelancer, if you have it open.
2. Run FLMPIDGEN.exe
3. Change the output format to 'Registry Datei (.reg)'
4. Click generate, and save the file somewhere you can find it. It doesn't matter what you call this.
5. Double click the file, and allow Windows to add the values to your registry. You should now have a functional multiplayer key

## CREDIT

Without the help of these individuals, this mod would not have been possible:

* Adoxa: For developing an enormous number of fixes and hacks included in this mod, as well as the production of JFLP.
* Amber Dickman: For producing some beautiful weapon FX used in this mod.
* IGx89: For creating the 'Fresh Start' mod included with FLMM, which the script for this mod uses elements of.
* Jeider: For creating the [Advanced Widescreen HUD](https://www.moddb.com/mods/freelacer-advanced-widescreen-hud) which is integrated into this mod.
* Raikkonen: For some serious assistance with FLHook.
* Why485 and the Flak 88 team: For allowing us to use their improved NPC AI and excellent custom missle VFX.

## CONTRIBUTING

* When submitting issues, please upload your copy of FLSpew.txt immediately after experiencing the issue or crash (If it's relevant). FLSpew can be found at ```C:\Users\Username\AppData\Local\Freelancer```.
* Please push strings to 0.92-second-circle as fast as possible (i.e. only one person should be working on .dll files at a time)