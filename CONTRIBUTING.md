## Contributing to ENE Freelancer

The official repository for this project is on [GitLab.com](https://gitlab.com/IrateRedKite/ene-freelancer).

* [Development](https://gitlab.com/IrateRedKite/ene-freelancer/-/blob/0.92-second-circle/Readme.md)
* [Issues](https://gitlab.com/IrateRedKite/ene-freelancer/issues)
* [Merge Requests](https://gitlab.com/IrateRedKite/ene-freelancer/merge_requests)

## Contributor license agreement

By submitting code as an individual you agree to the
[individual contributor license agreement](https://gitlab.com/IrateRedKite/ene-freelancer/-/blob/0.92-second-circle/LICENSE.txt).
By submitting code as an entity you agree to the
[corporate contributor license agreement](https://gitlab.com/IrateRedKite/ene-freelancer/-/blob/0.92-second-circle/LICENSE.txt).
